package com.note.developinghabits

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.nfc.Tag
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.activityViewModels
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.textfield.TextInputLayout
import kotlin.math.abs
import kotlin.random.Random

const val TAG = "AddHabits"
class AddHabits : Fragment(){

    //private val repository =HabitsRepository.get()
    private lateinit var inputHabits: TextInputLayout
    private lateinit var purposeHabits: TextInputLayout
    private lateinit var cardNotifications: com.google.android.material.card.MaterialCardView
    private lateinit var cardRepetitions: com.google.android.material.card.MaterialCardView
    private lateinit var infoNotificationsTextView:TextView
    private lateinit var infoRepetitions: TextView
    private lateinit var materialToolbar: MaterialToolbar
    private lateinit var appBarLayoutMain: AppBarLayout
    private  var oY: Float? = null
    private val model by activityViewModels<CustomViewModel>()



    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_add_habits, container, false)
        inputHabits = view.findViewById(R.id.input_habits)
        purposeHabits = view.findViewById(R.id.purpose_habits)
        cardNotifications = view.findViewById(R.id.card_notifications)
        cardRepetitions = view.findViewById(R.id.card_repetitions)
        infoNotificationsTextView = view.findViewById(R.id.info_notifications)
        infoRepetitions = view.findViewById(R.id.info_repetitions)
        materialToolbar = view.findViewById(R.id.topAppBar)





        //infoRepetitions.setOnTouchListener(this)
        return view
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        materialToolbar.setOnMenuItemClickListener{ menuItem
            -> when(menuItem.itemId){
                R.id.save ->{
                    val habits = DataHabits(id=Random.nextInt(),
                        nameHabits=inputHabits.editText?.text.toString(),
                        infoHabits=purposeHabits.editText?.text.toString(),
                        repetitions= infoRepetitions.text.toString().substringBefore(' ').toInt(),
                        notifications= infoNotificationsTextView.text.toString()=="вкл", doneHabits= false)
                    model.addHabit(habits)

                }
            }
            Toast.makeText(requireContext(), "Успешно сохранено", Toast.LENGTH_SHORT).show()
            true

        }

        cardNotifications.setOnClickListener {
            infoNotificationsTextView.text = if(infoNotificationsTextView.text.toString() == "выкл"){
                "вкл"
            }else{
                "выкл"
            }
        }




        infoRepetitions.setOnLongClickListener {
            Log.e(TAG, "долгое нажатие")
            infoRepetitions.setOnTouchListener { _, ev ->

                if(oY==null){
                    oY = ev?.y
                }

                if(ev?.y!! < oY!!){
                    val str = infoRepetitions.text.toString()
                    val number = str.substringBefore(' ').toInt()
                    infoRepetitions.text = "${(number + 1).toString()} раз"
                }else{
                    val str = infoRepetitions.text.toString()
                    val number = str.substringBefore(' ').toInt()
                    if(number != 0){
                        infoRepetitions.text = "${(number - 1).toString()} раз"
                    }
                }
                oY = ev.y
                false
            }

            true
        }



    }



}