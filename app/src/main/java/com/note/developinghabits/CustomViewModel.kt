package com.note.developinghabits

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

class CustomViewModel : ViewModel() {
    private var repository: HabitsRepository = HabitsRepository.get()
    var habitsList = repository.getHabits()
    var habits: DataHabits? = null


    fun getListHabits():List<DataHabits>?{

        return repository.getHabits().value
    }

    fun updateHabit(habit: DataHabits){
        repository.updateHabit(habit)
    }

    fun addHabit(habits: DataHabits){
        repository.addHabits(habits)
    }

    fun deleteHab(habits: DataHabits){
        repository.deleteHab(habits)
    }


}