package com.note.developinghabits

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class DataHabits(@PrimaryKey val id: Int,
                      var nameHabits: String,
                      var infoHabits: String,
                      var repetitions: Int,
                      var notifications: Boolean,
                      var doneHabits: Boolean)
