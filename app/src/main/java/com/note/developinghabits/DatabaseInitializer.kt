package com.note.developinghabits

import android.app.Application

class DatabaseInitializer : Application() {
    override fun onCreate() {
        super.onCreate()
        HabitsRepository.initialize(this)
    }
}