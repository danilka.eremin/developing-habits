package com.note.developinghabits

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.ContentFrameLayout
import androidx.fragment.app.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.R.id
import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.DialogInterface
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.ViewCompat
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import java.util.*
import kotlin.collections.ArrayList
import android.view.animation.Animation
import android.view.MotionEvent

import android.view.GestureDetector.SimpleOnGestureListener
import android.view.GestureDetector
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar


const val TAGS = "Habits"
class Habits : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var floatButton: FloatingActionButton
    private lateinit var textView2323: TextView


    private lateinit var dataSet: List<DataHabits>
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var toolbar: MaterialToolbar
    private lateinit var appBarLayout: AppBarLayout


    private val model by activityViewModels<CustomViewModel>()


    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_habits, container, false)
        floatButton = view.findViewById<FloatingActionButton>(R.id.floating_action_button)

        floatButton.setOnClickListener {
            Toast.makeText(requireContext(), "Тык", Toast.LENGTH_SHORT).show()
            addHab(view)
        }
        bottomNavigation = requireActivity().findViewById(R.id.bottom_navigation)
        if(bottomNavigation.visibility == View.INVISIBLE){
            bottomNavigation.visibility = View.VISIBLE
        }
        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        update(arrayListOf())

        model.habitsList.observe(viewLifecycleOwner, { x -> update(x) })

        toolbar = view.findViewById(R.id.topAppBar_Done)
        appBarLayout = view.findViewById(R.id.appBarLayout)
        if(appBarLayout.visibility == View.GONE){
            appBarLayout.visibility = View.VISIBLE
        }

        recyclerView.adapter = CustomAdapter()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setOnMenuItemClickListener { menuItem ->
            when(menuItem.itemId){
                R.id.done_habits ->{
                    val list = model.habitsList
                    if(list.value?.size == 0){
                        Toast.makeText(requireContext(), "Нет готовых привычек", Toast.LENGTH_SHORT)
                            .show()
                        false
                    }
                    for(i in list.value!!){
                        if(i.doneHabits){
                            model.deleteHab(i)
                        }
                    }
                }
            }
            true
        }
    }


    override fun onPause() {
        appBarLayout.visibility = View.GONE
        super.onPause()
    }

    private fun update(elem: List<DataHabits>) {

        dataSet = elem
        recyclerView.adapter = CustomAdapter()
    }


    private fun addHab(v: View){


        parentFragmentManager.commit {
            val nextFrag = AddHabits()
            replace(R.id.fragment_container_view, nextFrag)
            addToBackStack(null)
        }

    }



    inner class CustomAdapter() :
        RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view),
            View.OnClickListener {
            val labelHabits: TextView = view.findViewById(R.id.textView231)
            val infoHabits: TextView = view.findViewById(R.id.textView2)
            val card: com.google.android.material.card.MaterialCardView = view.findViewById(R.id.card)

            lateinit var hab: DataHabits

            init {
                textView2323 = view.findViewById(R.id.textView231)
                view.setOnClickListener(this)

                card.setOnLongClickListener{
                    card.isChecked = !card.isChecked
                    hab.doneHabits = card.isChecked

                    //log()
                    true
                }
            }

            fun log(){
                for(i in dataSet){
                    Log.i(TAGS,
                        "Информация - ${i.infoHabits}. " +
                                "Задача - ${i.nameHabits}. " +
                                "Выполнение- ${i.doneHabits}. " +
                                "Уведомления - ${i.notifications}. " +
                                "Повторения -  ${i.repetitions}")
                }
            }

            override fun onClick(v: View?) {


                //Toast.makeText(requireContext(), "АААААА", Toast.LENGTH_SHORT).show()
                //ViewCompat.setTransitionName(textView2323, "item")

                model.habits = hab
                val fragment = InfoHabits()
                parentFragmentManager.commit {
                    //addSharedElement(textView2323, "item")
                    replace(R.id.fragment_container_view, fragment)
                    addToBackStack(null)
                }


            }



            fun bind(habits: DataHabits){
                hab = habits
                card.isChecked = hab.doneHabits
            }
        }


        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {

            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.text_row_item, viewGroup, false)

            return ViewHolder(view)
        }


        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
            viewHolder.apply {
                bind(dataSet[position])
                labelHabits.text = dataSet[position].nameHabits
                infoHabits.text = dataSet[position].infoHabits
            }
        }

        override fun getItemCount() = dataSet.size



    }
}


