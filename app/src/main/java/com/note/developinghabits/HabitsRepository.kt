package com.note.developinghabits

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.Room
import com.note.developinghabits.database.AppDatabase
import java.util.*
import java.util.concurrent.Executors


private const val DATABASE_NAME = "task-database"
class HabitsRepository(context: Context) {


    val database = Room.databaseBuilder(
        context.applicationContext,
        AppDatabase::class.java,
        DATABASE_NAME
    ).build()

    private val habitsDao = database.userDao()
    private val executor = Executors.newSingleThreadExecutor()


    fun addHabits(Habits: DataHabits){
        executor.execute {
            habitsDao.addHabits(Habits)
        }
    }
    fun updateHabit(Habits: DataHabits){
        executor.execute {
            habitsDao.update(Habits)
        }

    }

    fun deleteHab(habits: DataHabits){
        executor.execute {
            habitsDao.delete(habits)
        }
    }

    fun getHabits(): LiveData<List<DataHabits>> = habitsDao.getAll()
    fun getHabit(id: Int): LiveData<DataHabits?> = habitsDao.loadAllByIds(id)

    companion object{
        private var INSTANCE: HabitsRepository? = null

        fun initialize(context: Context) {
            if (INSTANCE == null) {
                INSTANCE = HabitsRepository(context)
            }
        }

        fun get(): HabitsRepository {
            return INSTANCE ?:
            throw IllegalStateException("CrimeRepository must be initialized")
        }
    }
}