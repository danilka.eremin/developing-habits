package com.note.developinghabits

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.transition.TransitionInflater
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.card.MaterialCardView
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.WeekFields
import java.util.*

const val Tag = "InfoHabits"
class InfoHabits : Fragment() {

    private var labelHabits: TextView? = null
   // private var repository = HabitsRepository.get()
    private lateinit var calendarView: com.kizitonwose.calendarview.CalendarView
    private val today = LocalDate.now()
    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var card: MaterialCardView
    private lateinit var repetitions: TextView

    private lateinit var  objectiveHabits:TextView
    private var boolean: Boolean = false

    private val model by activityViewModels<CustomViewModel>()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_info_habits, container, false)
        labelHabits = view.findViewById(R.id.check)
        objectiveHabits = view.findViewById(R.id.objective_habits)
        bottomNavigation = requireActivity().findViewById(R.id.bottom_navigation)
        bottomNavigation.visibility = View.INVISIBLE
        repetitions = view.findViewById(R.id.repetitions)



        labelHabits?.text = model.habits?.nameHabits
        objectiveHabits.text = model.habits?.infoHabits
        repetitions.text = model.habits?.repetitions.toString()

        return view
    }
    /**первое правило. если работает, но ты не знаешь как оно работает -- не трогай*/
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        calendarView = view.findViewById(R.id.calendarView)
        val daysOfWeek = daysOfWeekFromLocale()



        val currentMonth = YearMonth.now()
        val startMonth = currentMonth.minusMonths(10)
        val endMonth = currentMonth.plusMonths(10)
        /*не трогай то что работает*/
        calendarView.apply {
            setup(startMonth, endMonth, daysOfWeek.first())
            scrollToMonth(currentMonth)
        }

        calendarView.dayBinder = object : DayBinder<DayViewContainer> {
            // Called only when a new container is needed.
            override fun create(view: View) = DayViewContainer(view)

            // Called every time we need to reuse a container.
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView

                textView.text = day.date.dayOfMonth.toString()
                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.apply {
                        visibility = View.VISIBLE
                        setTextColor(Color.WHITE)
                    }

                    if(today==day.date){
                        textView.apply {
                            setTextColor(Color.WHITE)
                            setBackgroundResource(R.drawable.test)
                        }

                    }

                    textView.apply {
                        visibility = View.VISIBLE
                        setTextColor(Color.WHITE)
                    }

                    if (day.date == container.selectedDate && day.date != today) {
                        // If this is the selected date, show a round background and change the text color.

                        textView.setBackgroundResource(R.drawable.selection_background)
                    } else {
                        // If this is NOT the selected date, remove the background and reset the text color.

                        if(boolean){
                            textView.background = null
                            boolean = false
                        }

                    }
                } else {
                    // Hide in and out dates
                    textView.visibility = View.INVISIBLE
                }
            }
        }

        calendarView.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                container.textView.text = "${month.yearMonth.month.name.toLowerCase().capitalize()} ${month.year}"
            }
        }


    }

    inner class DayViewContainer(view: View) : ViewContainer(view) {
        val textView = view.findViewById<TextView>(R.id.calendarDayText)

        lateinit var day: CalendarDay
        var selectedDate: LocalDate? = null

        init {
            view.setOnClickListener {
                val currentSelection = selectedDate
                if (currentSelection == day.date) {
                    // If the user clicks the same date, clear selection.
                    // selectedDate = null
                    // Reload this date so the dayBinder is called
                    // and we can REMOVE the selection background.
                    //calendarView.notifyDateChanged(currentSelection)
                } else {
                    selectedDate = day.date
                    // Reload the newly selected date so the dayBinder is
                    // called and we can ADD the selection background.
                    calendarView.notifyDateChanged(day.date)
                    if (currentSelection != null) {
                        // We need to also reload the previously selected
                        // date so we can REMOVE the selection background.
                        calendarView.notifyDateChanged(currentSelection)
                    }
                }
            }
            view.setOnLongClickListener {
                boolean = true
                val currentSelection = selectedDate
                if (currentSelection == day.date) {
                    // If the user clicks the same date, clear selection.
                    selectedDate = null
                    // Reload this date so the dayBinder is called
                    // and we can REMOVE the selection background.
                    calendarView.notifyDateChanged(currentSelection)
                }

                true
            }
        }


        // With ViewBinding
        // val textView = CalendarDayLayoutBinding.bind(view).calendarDayText
    }



    class MonthViewContainer(view: View) : ViewContainer(view) {
        val textView = view.findViewById<TextView>(R.id.headerTextView)
    }


    fun daysOfWeekFromLocale(): Array<DayOfWeek> {
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        val daysOfWeek = DayOfWeek.values()
        // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
        // Only necessary if firstDayOfWeek is not DayOfWeek.MONDAY which has ordinal 0.
        if (firstDayOfWeek != DayOfWeek.MONDAY) {
            val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
            val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
            return rhs + lhs
        }
        return daysOfWeek
    }

}