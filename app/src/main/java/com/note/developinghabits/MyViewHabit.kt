package com.note.developinghabits

import android.content.Context
import android.util.AttributeSet
import android.view.View

class MyViewHabit(context: Context, attrs: AttributeSet) : View(context, attrs){

    init {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.MyViewHabit,
            0, 0).apply {

            try {
                mShowText = getBoolean(R.styleable.MyViewHabit_showText, false)
                textPos = getInteger(R.styleable.MyViewHabit_labelPosition, 0)
            } finally {
                recycle()
            }
        }
    }

}