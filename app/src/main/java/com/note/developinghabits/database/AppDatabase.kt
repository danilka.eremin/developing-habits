package com.note.developinghabits.database

import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.RoomDatabase
import com.note.developinghabits.DataHabits

@Database(version = 3, entities = [DataHabits::class])
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): HabitsDao
}