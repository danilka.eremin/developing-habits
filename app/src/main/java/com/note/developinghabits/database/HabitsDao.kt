package com.note.developinghabits.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.note.developinghabits.DataHabits
import java.util.*


@Dao
interface HabitsDao {
    @Query("SELECT * FROM dataHabits")
    fun getAll(): LiveData<List<DataHabits>>

    @Query("SELECT * FROM dataHabits WHERE id IN (:userIds)")
    fun loadAllByIds(userIds: Int): LiveData<DataHabits?>

    @Insert
    fun addHabits(habits: DataHabits)

    @Insert
    fun insertAll(vararg users: DataHabits)

    @Update
    fun update(habits: DataHabits)

    @Delete
    fun delete(habit: DataHabits)
}